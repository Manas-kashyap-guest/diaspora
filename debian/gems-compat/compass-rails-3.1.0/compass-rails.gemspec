# -*- encoding: utf-8 -*-
require File.expand_path('../lib/compass-rails/version', __FILE__)

Gem::Specification.new do |gem|
  gem.authors       = ["Scott Davis", "Chris Eppstein", "Craig McNamara"]
  gem.email         = ["jetviper21@gmail.com", "chris@eppsteins.net", "craig.mcnamara@gmail.com"]
  gem.description   = %q{Integrate Compass into Rails 3.0 and up.}
  gem.summary       = %q{Integrate Compass into Rails 3.0 and up.}
  gem.homepage      = "https://github.com/Compass/compass-rails"

  gem.executables   = gem.files.select {|v| v =~ /^bin\//}.map { |f| File.basename(f) }
  gem.files         = Dir.glob("**/*").select {|v| v !~ /^debian/}
  gem.test_files    = gem.files.select {|v| v =~ /^(test|spec|features)/}
  gem.name          = "compass-rails"
  gem.require_paths = ["lib"]
  gem.version       = CompassRails::VERSION
  gem.license       = "MIT"

  gem.add_dependency 'compass',    '~> 1.0.0'
  gem.add_dependency 'sprockets',  '< 4.0'
  gem.add_dependency 'sass-rails', '< 5.1'
end
